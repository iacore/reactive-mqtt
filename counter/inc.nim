#!/usr/bin/env -S nim r --verbosity:0

import nmqtt, std/[asyncdispatch]

proc main() {.async.} =
  let ctx = newMqttCtx("inc.nim")
  ctx.set_host("localhost", 1883)
  await ctx.start()
  await ctx.publish("/inc", "inc", 2)
  await sleepAsync 500

waitFor main()
echo "Done. Check log/counter.log"

