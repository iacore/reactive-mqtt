#!/usr/bin/env -S nim r --verbosity:0

import nmqtt, std/[strformat, asyncdispatch]

var count = 0
proc on_inc(topic: string, message: string) =
  count += 1
  echo &"Count: {count}"

proc main {.async.} =
  let ctx = newMqttCtx("counter.nim")
  ctx.set_host("localhost", 1883)
  await ctx.start()
  await ctx.subscribe("/inc", 2, on_inc)

echo "Started"
waitFor main()
runForever()

