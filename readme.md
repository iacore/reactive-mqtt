If the smallest restartable unit in UNIX is a process, then an reactive application should have mulitple processes to be updated individually.

Since MQTT can retain last state, or set value when client dies, it is useful for creating reactive systems.

Of course, this might be too many moving parts to you. A production system may not rely on centralizer message broker, but instead use a better message broker.

Components:
- dinit, as process dependency/restart manager
- mosquitto (MQTT broker), as message bus
- individual applications, each one a UNIX process

Useful tools:

[MQTT Explorer](https://github.com/thomasnordquist/MQTT-Explorer/) - see MQTT messages

